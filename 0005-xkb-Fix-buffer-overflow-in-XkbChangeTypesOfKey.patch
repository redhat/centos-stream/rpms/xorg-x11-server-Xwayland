From 39c7ed837bf1d6b5b45fd870bbdb15870c76b87f Mon Sep 17 00:00:00 2001
From: Olivier Fourdan <ofourdan@redhat.com>
Date: Thu, 28 Nov 2024 14:09:04 +0100
Subject: [PATCH xserver 05/13] xkb: Fix buffer overflow in
 XkbChangeTypesOfKey()

If XkbChangeTypesOfKey() is called with nGroups == 0, it will resize the
key syms to 0 but leave the key actions unchanged.

If later, the same function is called with a non-zero value for nGroups,
this will cause a buffer overflow because the key actions are of the wrong
size.

To avoid the issue, make sure to resize both the key syms and key actions
when nGroups is 0.

CVE-2025-26597, ZDI-CAN-25683

This vulnerability was discovered by:
Jan-Niklas Sohn working with Trend Micro Zero Day Initiative

Signed-off-by: Olivier Fourdan <ofourdan@redhat.com>
Reviewed-by: Peter Hutterer <peter.hutterer@who-t.net>
(cherry picked from commit 0e4ed94952b255c04fe910f6a1d9c852878dcd64)

Part-of: <https://gitlab.freedesktop.org/xorg/xserver/-/merge_requests/1831>
---
 xkb/XKBMisc.c | 1 +
 1 file changed, 1 insertion(+)

diff --git a/xkb/XKBMisc.c b/xkb/XKBMisc.c
index 0a3b5cbc7..4e89b5bdf 100644
--- a/xkb/XKBMisc.c
+++ b/xkb/XKBMisc.c
@@ -552,6 +552,7 @@ XkbChangeTypesOfKey(XkbDescPtr xkb,
         i = XkbSetNumGroups(i, 0);
         xkb->map->key_sym_map[key].group_info = i;
         XkbResizeKeySyms(xkb, key, 0);
+        XkbResizeKeyActions(xkb, key, 0);
         return Success;
     }
 
-- 
2.48.1

